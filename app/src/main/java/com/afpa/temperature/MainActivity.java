package com.afpa.temperature;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.afpa.temperature.utils.TemperatureConverter;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private EditText editTextCelsius;
    private EditText editTextFahrenheit;
    private ListView listViewTemperature;
    List<String> stringList = new ArrayList<> ();
    private ArrayAdapter<String> adapter;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //affichage du menu (R.menu.menu)
        getMenuInflater ().inflate (R.menu.menu, menu);

        return super.onCreateOptionsMenu (menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId ()) {
            case R.id.menu_delate:
                editTextFahrenheit.getText ().clear ();
                editTextCelsius.getText ().clear ();

                //editTextFahrenheit.setText ("");
                //editTextCelsius.setText ("");


                // TODO : effacer les champs Celsius / Fahrenheit
                // TODO : effacer les items de la listViewTemperature
                break;

        }

        return super.onOptionsItemSelected (item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);

        editTextCelsius = (EditText) findViewById (R.id.editTextCelsius);
        editTextFahrenheit = (EditText) findViewById (R.id.editTextFahrenheit);
        listViewTemperature = (ListView) findViewById (R.id.listViewTemperature);

        editTextFahrenheit.addTextChangedListener (new TextWatcher () {
            // methode TextWatcher permet // d'ecouter ce qui se passe dans la editText
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                       @Override
                       public void onTextChanged(CharSequence s, int start, int before, int count) {
                       }

                       @Override
                       public void afterTextChanged(Editable s) {
                           String valeur = s.toString ();

                           if (editTextFahrenheit.hasFocus ()) {
                               if (!valeur.isEmpty () && TemperatureConverter.isNumeric (valeur)) {
                                   //if(valeur.length () > 0){ 2ème synthaxe possible pour vérifier le champs vide
                                   String resultat = TemperatureConverter.celsiusFromFahrenheit (Double.valueOf (valeur));

                                   editTextCelsius.setText (resultat);

                                   Log.e ("Celsius", "Affichage valeur" + resultat);
                               }else {
                                   editTextCelsius.setText ("");
                               }
                           }
                       }
                   }

        );

     editTextCelsius.addTextChangedListener (new TextWatcher () { // methode TextWatcher permet
            // d'ecouter ce qui se passe dans la editText
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // String valeur = s.toString ();

                if (editTextCelsius.hasFocus ()) {
                    String valeur = s.toString ();
                    if (!valeur.isEmpty () && TemperatureConverter.isNumeric (valeur)) {
                        //if(valeur.length () > 0){ 2ème synthaxe possible pour vérifier le champs vide
                        String resultat = TemperatureConverter.fahrenheitFromCelcius (Double.valueOf (valeur));

                        editTextFahrenheit.setText (resultat);

                        Log.e ("Fahrenheit", "Affichage valeur" + resultat);

                    } else {
                        editTextFahrenheit.setText ("");

                    }
                }
            }
        });

        adapter = new ArrayAdapter<String> (MainActivity.this,
//                // setAdapter permet de gérer la mise a jour de la liste d'object
                android.R.layout.simple_list_item_1, stringList);
        listViewTemperature.setAdapter (adapter);
        adapter.clear ();
    }

    public void save(View view) {

        if (!editTextCelsius.getText ().toString ().isEmpty () && !editTextFahrenheit.getText ()
                .toString ().isEmpty ()) {
            stringList.add (
                    String.format (getString (R.string.main_text_convert),
                            editTextCelsius.getText ().toString (),
                            editTextFahrenheit.getText ().toString ()
                    )
            );
            // rafraihissment
            adapter.notifyDataSetChanged ();
        }


        listViewTemperature.setOnItemClickListener (
                new AdapterView.OnItemClickListener () {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        String itemsListViewTemperature = stringList.get (position);
                    }
                }

        );
//
//
//
    }


}
